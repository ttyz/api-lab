#!/bin/bash

if [ ! -t 0 ]; then
    input=$(cat)
fi

collectiondir=${COLLECTION_DIR:-$(dirname $0)/collections}
params=$(cat $collectiondir/${1}.jq | grep 'def _params:' )
preexec=$(echo $input | bash -c "jq -r \
    -L$collectiondir \
    -L$(dirname $0) \
    $(
        profile="${PROFILE_DIR:-$(dirname $0)/profiles}/${3:-default}.json";
        [ -f "${profile}" ] && echo -n " --slurpfile profile ${profile}"
        [ ! -f "${profile}" ]&&  echo -n  " --argjson profile []"
    ) \
    'import \"${1}\" as c;
        (\$profile[0]) $([ -n "${params}" ] && echo -n ' + c::_params') + . |
        c::${2:-default} | .preexec // \"\"'" \"
)

if [ -n "${preexec}" ]; then
    echo "Running prexec script: '${preexec}'";
    input=$(bash -c "${preexec}" | jq -r "${input} + .")
fi

if [ -z "${input}" ]; then
    input="null"
fi

params=$(cat $collectiondir/${1}.jq | grep 'def _params:' )

jq_command="jq -r \
    -L$collectiondir \
    -L$(dirname $0) \
    --arg RANDOM ${RANDOM} \
    $(
        profile="${PROFILE_DIR:-$(dirname $0)/profiles}/${3:-default}.json";
        [ -f "${profile}" ] && echo -n " --slurpfile profile ${profile}"
        [ ! -f "${profile}" ]&&  echo -n  " --argjson profile []"
    ) \
    --arg env ${ENVIRONMENT:-local} \
    --arg extra \"tee /tmp/last-response.html\" \
    'import \"${1}\" as c; import \"process\" as p;
        (\$profile[0]) $([ -n "${params}" ] && echo -n ' + c::_params') + . |
        c::${2:-default} |
        p::getenv(c::_envs; \$env) as \$environment |
        p::process(\$environment; \$extra)'"

command=$(echo ${input} | bash -c "$jq_command")

bash -c "${command}"
