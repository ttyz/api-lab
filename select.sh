#!/bin/bash
set -euo pipefail
dir=$(dirname $0)

collection=$(ls --color=never ${dir}/collections/* | xargs  basename -s .jq | dmenu)
export ENVIRONMENT=$(jq -rn -L${dir}/collections "import \"${collection}\" as c; c::_envs | keys | .[] | select(. != \"*\")" | dmenu)
request=$(cat collections/${collection}.jq | sed -n 's/def \(.*\):/\1/p' | egrep -v '^_' | dmenu)
profile=$(ls --color=never ${dir}/profiles/* | xargs  basename -s .json | dmenu)

echo "Executing ${collection}.${request} with ${profile} profile on ${ENVIRONMENT} environment"
echo
${dir}/run_api.sh ${collection} ${request} ${profile}
