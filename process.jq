def encode(encoder):
{
    "json": (. | @json | @sh),
} | .[encoder];

def content(executor; contentType):
if .data.params then
{
    "curl": {
        "json": {"args": (.args + ["-d\(.data.params | encode("json"))"])},
        "formdata" : {"args": ( .args + [ .data.params | to_entries[] | "-F'\(.key)=\(.value)'" ])},
    }
} | .[executor][contentType] // .[ executor ] // {}
else
{}
end;

def auth(executor):
if .environment.auth then
{
    "curl": {"args": (.args + ["-u\(.environment.auth)"])},
    "browser": {"url": (.environment.auth as $auth | .url | sub("://"; "://\($auth)@"))}
} | .[ executor ] // {}
else
{}
end;

def exec(executor):
{
    "curl": "curl -sv -X\(.method)\(if .headers then " -H \(.headers | to_entries[] | "\(.key): \(.value)" | @sh)" else "" end) \(.args | join(" ")) '\(.url)' 2> >(sed '/^* /d; /bytes data]$/d; s/> //; s/< //; /^ *CApath:/ d' >&2) | \(.formatter)",
    "browser": "\(env.BROWSER) '\(.url)'",
} | .[ executor ];


def getenv(envs; environment):
envs["*"] + envs[environment // "local"];

def process(environment; extra):
( environment.url | sub("/$"; "")) as $url |
( .executor // "curl" ) as $executor |
( .content // "json" ) as $contentType |
{
    "url": ($url + "/" + (.endpoint // "" | sub("^/"; ""))),
    "method": (.method // "GET" | ascii_upcase),
    "formatter": ([ extra // empty, (.formatter // "jq -C '.'")] | join(" | ")),
    "environment": environment,
    "headers": (.headers // null),
    "args": [],
    "data": .,
} |
. + auth($executor) |
. + content($executor; $contentType) |
exec($executor);

def process(environment):
process(environment; null);

