set_expectation() {
    curl -sXPUT -d"${1}" ${MOCK_SERVER_URL:-http://localhost:1080}/expectation
}

reset_mock() {
    curl -sXPUT ${MOCK_SERVER_URL:-http://localhost:1080}/reset
}

verify_expectation() {
    local code=$(curl -sXPUT -d"${1}" -o /dev/null -w "%{http_code}" ${MOCK_SERVER_URL:-http://localhost:1080}/verify)
    [ "${code}" == "202" ] && return 0
    >&2 echo "Verification code: ${code}"
    return ${code}
}

verify_mock_called() {
    local last_call_args=$(cat /tmp/test-call-args)
    rm -rf /tmp/test-call-args
    [ "${@}" != "${last_call_args}" ] && \
        echo "'${@}' was expected, got '${last_call_args}'" && \
        return 1

    return 0
}
