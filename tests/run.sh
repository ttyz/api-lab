#!/bin/bash

GRAY="\033[1;30m"
LIGHT_GRAY="\033[0;37m"
GREEN="\033[0;32m"
LIGHT_GREEN="\033[1;32m"
RED="\033[0;31m"
LIGHT_RED="\033[1;31m"
CYAN="\033[0;36m"
LIGHT_CYAN="\033[1;36m"
NO_COLOUR="\033[0m"

source $(dirname $0)/helpers.sh

sleep 1
echo

total=0
fail=0

for file in $(ls ${1:-$(dirname $0)}/test*.sh); do
    source ${file}
    tests=($(grep -o '^_test[^(]*' ${file}))
    for test in "${tests[@]}"; do
        total=$((total+1))
        printf "${GRAY}%3d:${NO_COLOUR} %-30s${NO_COLOUR}" "${total}" "${test}"
        set_up
        err=$(set -e; ${test} 2>&1);
        rc=$?
        reset_mock
        tear_down
        if [ "${rc}" != 0 ]; then
            echo -e "${LIGHT_RED}FAIL${NO_COLOUR}\n$err\n"
            # docker-compose logs --tail=45
            fail=$((fail+1))
            continue
        fi

        echo -e "${LIGHT_GREEN}PASS${NO_COLOUR}"
    done
done

echo

[ ${fail} -gt 0 ] && echo -e "${LIGHT_RED}${fail} of ${total}${RED} Tests failed${NO_COLOUR}" && exit 1
echo -e "${LIGHT_GREEN}All tests passed${NO_COLOUR}"
