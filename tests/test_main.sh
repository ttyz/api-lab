#!/bin/bash

source $(dirname $0)/helpers.sh

set_up() {
    tmpcollectiondir=/tmp/collection${RANDOM}
    mkdir -p ${tmpcollectiondir}
    mkdir -p ${tmpcollectiondir}/profiles
    rm -rf /tmp/last-response
    rm -rf /tmp/test-output
}

tear_down() {
    rm -rf ${tmpcollectiondir}
    rm -rf /tmp/last-response
}

_test_param_preexec() {
    set_expectation '{"httpRequest":{"path":"/test/value"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "local": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", }
        };

        def default:
        {
            "preexec": "jq -nr '{\"testexec\": \"\(.test)\"}'",
            "endpoint": "test/\\(.testexec)"
        };
	EOF

    local ENVIRONMENT=
    echo "{\"test\":\"value\"}" | \
    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection

    verify_expectation '{"httpRequest":{"path":"/test/value"},"times":{"atLeast":1,"atMost":1}}'
}

_test_stdin_preexec() {
    set_expectation '{"httpRequest":{"path":"/test/value"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "local": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", }
        };

        def default:
        {
            "preexec": "jq -nr '{\"testexec\": \"value\"}'",
            "endpoint": "\\(.test)/\\(.testexec)"
        };
	EOF

    local ENVIRONMENT=
    echo "{\"test\":\"test\"}" | \
    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection

    verify_expectation '{"httpRequest":{"path":"/test/value"},"times":{"atLeast":1,"atMost":1}}'
}

_test_header() {
    set_expectation '{"httpRequest":{"path":"/test"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "local": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", }
        };

        def default:
        {
            "endpoint": "test",
            "headers": {
                "X-TEST": "test-header",
            },
        };
	EOF
    local ENVIRONMENT=
    local output=$(COLLECTION_DIR=${tmpcollectiondir} \
        PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection)


    verify_expectation '{"httpRequest":{"path":"/test","headers":{"X-TEST":["test-header"]}},"times":{"atLeast":1,"atMost":1}}'
}

_test_browser() {
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "local": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", }
        };

        def default:
        {
            "endpoint": "test",
            "executor": "browser",
        };
	EOF
    local ENVIRONMENT=
    BROWSER=">/tmp/test-call-args echo" \
    COLLECTION_DIR=${tmpcollectiondir} \
        PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection

     verify_mock_called ${MOCK_SERVER_URL:-http://localhost:1080}/test
     echo $?
}

_test_httpout() {
    set_expectation '{"httpRequest":{"path":"/test"},"httpResponse":{"body":"<h1>ok</h1>"}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "local": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", }
        };

        def default:
        {
            "formatter": "cat",
            "endpoint": "test"
        };
	EOF
    local ENVIRONMENT=
    COLLECTION_DIR=${tmpcollectiondir} \
        PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection > ${tmpcollectiondir}/testout 2>&1

    [ -z "$(cat ${tmpcollectiondir}/testout | grep 'GET /test HTTP/1.1')" ] && \
    [ -z "$(cat ${tmpcollectiondir}/testout | grep 'HTTP/1.1 200 OK')" ] && \
        echo "Headers not found in the output:\n$(cat ${tmpcollectiondir}/testout)" && \
        exit 1
    return 0
}

_test_default_params() {
    set_expectation '{"httpRequest":{"path":"/test/1"},"httpResponse":{"body":"<h1>ok</h1>"}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _params:
        {
            "testval": 1,
        };

        def _envs:
        {
            "local": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", }
        };

        def default:
        {
            "endpoint": "test/\\(.testval)"
        };
	EOF
    local ENVIRONMENT=
    local output=$(COLLECTION_DIR=${tmpcollectiondir} \
        PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection)


    verify_expectation '{"httpRequest":{"path":"/test/1"},"times":{"atLeast":1,"atMost":1}}'
}

_test_nonjson_content() {
    set_expectation '{"httpRequest":{"path":"/test"},"httpResponse":{"body":"<h1>ok</h1>"}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "local": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", }
        };

        def default:
        {
            "formatter": "base64",
            "endpoint": "test"
        };
	EOF
    local ENVIRONMENT=
    local output=$(COLLECTION_DIR=${tmpcollectiondir} \
        PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection)

    [ ! -f "/tmp/last-response.html" ] && echo "No response" && return 1
    local result=$(cat /tmp/last-response.html)
    expected=$(echo -n '<h1>ok</h1>' | base64)

    [ "${output}" != "${expected}" ] && echo "Wrong output: ${output}" && echo "Expected:     ${expected}"  && return 1
    [ "${result}" != '<h1>ok</h1>' ] && echo "Wrong result: ${result}" && return 1
    return 0
}

_test_content() {
    set_expectation '{"httpRequest":{"path":"/test"},"httpResponse":{"body":"\"ok ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "local": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", }
        };

        def default:
        {
            "endpoint": "test"
        };
	EOF
    local ENVIRONMENT=
    local result=$(COLLECTION_DIR=${tmpcollectiondir} \
        PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection 2>/dev/null)

    [ "${result}" != $(echo '"ok ok"' | jq -C '.') ] && echo "Wrong output: ${result}" && return 1
    return 0
}

_test_stdin_params() {
    set_expectation '{"httpRequest":{"path":"/test"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _params:
        {
            "test": "default",
        };

        def _envs:
        {
            "local": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", }
        };

        def default:
        {
            "endpoint": .test
        };
	EOF

    local ENVIRONMENT=
    echo '{"test":"test"}' |\
    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection

    verify_expectation '{"httpRequest":{"path":"/test"},"times":{"atLeast":1,"atMost":1}}'
}

_test_stdin() {
    set_expectation '{"httpRequest":{"path":"/test"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "local": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", }
        };

        def default:
        {
            "endpoint": .test
        };
	EOF

    local ENVIRONMENT=
    echo '{"test":"test"}' |\
    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection

    verify_expectation '{"httpRequest":{"path":"/test"},"times":{"atLeast":1,"atMost":1}}'
}

_test_local_url_default_auth() {
    set_expectation '{"httpRequest":{"path":"/"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "*": { "auth": "test:test", },
            "local": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", }
        };

        def default:
        {};
	EOF

    local ENVIRONMENT=
    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection

    verify_expectation '{"httpRequest":{"path":"/","headers":{"Authorization":["Basic dGVzdDp0ZXN0"]}},"times":{"atLeast":1,"atMost":1}}'
}


_test_no_profile() {
    set_expectation '{"httpRequest":{"path":"/"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        { "*": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", } };

        def default:
        {};
	EOF

    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection

    verify_expectation '{"httpRequest":{"path":"/"},"times":{"atLeast":1,"atMost":1}}'
}


_test_default_request() {
    set_expectation '{"httpRequest":{"path":"/"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        { "*": { "url": "${MOCK_SERVER_URL:-http://localhost:1080}", } };

        def default:
        {};
	EOF

    cat <<- EOF > ${tmpcollectiondir}/profiles/default.json
        {}
	EOF

    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection

    verify_expectation '{"httpRequest":{"path":"/"},"times":{"atLeast":1,"atMost":1}}'
}

_test_local_env() {
    set_expectation '{"httpRequest":{"path":"/"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "local": {
                "url": "${MOCK_SERVER_URL:-http://localhost:1080}",
            }
        };

        def testrequest:
        {};
	EOF

    cat <<- EOF > ${tmpcollectiondir}/profiles/default.json
        {}
	EOF

    local ENVIRONMENT=
    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection testrequest

    verify_expectation '{"httpRequest":{"path":"/"},"times":{"atLeast":1,"atMost":1}}'
}

_test_default_env() {
    set_expectation '{"httpRequest":{"path":"/"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "*": {
                "url": "${MOCK_SERVER_URL:-http://localhost:1080}",
            },
            "google": {
                "url": "http://google.com",
            }
        };

        def testrequest:
        {};
	EOF

    cat <<- EOF > ${tmpcollectiondir}/profiles/default.json
        {}
	EOF

    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
        $(dirname $0)/../run_api.sh testcollection testrequest

    verify_expectation '{"httpRequest":{"path":"/"},"times":{"atLeast":1,"atMost":1}}'
}

_test_body() {
    set_expectation '{"httpRequest":{"path":"/"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "unittest": {
                "url": "${MOCK_SERVER_URL:-http://localhost:1080}",
            }
        };

        def testrequest:
        {
            "method": "post",
            "params": {
                "test": 1
            }
        };
	EOF

    cat <<- EOF > ${tmpcollectiondir}/profiles/default.json
        {}
	EOF

    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
    ENVIRONMENT=unittest \
        $(dirname $0)/../run_api.sh testcollection testrequest

    verify_expectation '{"httpRequest":{"path":"/","body":{"type":"JSON","json":"{\"test\":1}","matchType":"STRICT"}},"times":{"atLeast":1,"atMost":1}}'
}

_test_auth() {
    set_expectation '{"httpRequest":{"path":"/"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "unittest": {
                "url": "${MOCK_SERVER_URL:-http://localhost:1080}",
                "auth": "test:test"
            }
        };

        def testrequest:
        {};
	EOF

    cat <<- EOF > ${tmpcollectiondir}/profiles/default.json
        {}
	EOF

    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
    ENVIRONMENT=unittest \
        $(dirname $0)/../run_api.sh testcollection testrequest

    verify_expectation '{"httpRequest":{"path":"/","headers":{"Authorization":["Basic dGVzdDp0ZXN0"]}},"times":{"atLeast":1,"atMost":1}}'
}


_test_method() {
    set_expectation '{"httpRequest":{"path":"/test/1","method":"POST"},"httpResponse":{"body":"\"ok\""}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "unittest": {
                "url": "${MOCK_SERVER_URL:-http://localhost:1080}"
            }
        };

        def testrequest:
        {
            "endpoint": "test/1",
            "method": "post"
        };
	EOF

    cat <<- EOF > ${tmpcollectiondir}/profiles/default.json
        {}
	EOF

    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
    ENVIRONMENT=unittest \
        $(dirname $0)/../run_api.sh testcollection testrequest

    verify_expectation '{"httpRequest":{"path":"/test/1","method":"POST"},"times":{"atLeast":1,"atMost":1}}'
}

_test_slash_add() {
    set_expectation '{"httpRequest":{"path":"/test/1"},"httpResponse":{"body":"\"ok\""},"times":{"unlimited":true}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "unittest": {
                "url": "${MOCK_SERVER_URL:-http://localhost:1080}"
            }
        };

        def testrequest:
        { "endpoint": "test/1" };
	EOF

    cat <<- EOF > ${tmpcollectiondir}/profiles/default.json
        {}
	EOF

    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
    ENVIRONMENT=unittest \
        $(dirname $0)/../run_api.sh testcollection testrequest

    verify_expectation '{"httpRequest":{"path":"/test/1"},"times":{"atLeast":1,"atMost":1}}'
}

_test_slash_remove() {
    set_expectation '{"httpRequest":{"path":"/test/1"},"httpResponse":{"body":"\"ok\""},"times":{"unlimited":true}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "unittest": {
                "url": "${MOCK_SERVER_URL:-http://localhost:1080}/"
            }
        };

        def testrequest:
        { "endpoint": "/test/1" };
	EOF

    cat <<- EOF > ${tmpcollectiondir}/profiles/default.json
        {}
	EOF

    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
    ENVIRONMENT=unittest \
        $(dirname $0)/../run_api.sh testcollection testrequest

    verify_expectation '{"httpRequest":{"path":"/test/1"},"times":{"atLeast":1,"atMost":1}}'
}

_test_default_profile() {
    set_expectation '{"httpRequest":{"path":"/test"},"httpResponse":{"body":"\"ok\""},"times":{"unlimited":true}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "unittest": {
                "url": "${MOCK_SERVER_URL:-http://localhost:1080}"
            }
        };

        def testrequest:
        {
            "endpoint": .param
        };
	EOF

    cat <<- EOF > ${tmpcollectiondir}/profiles/default.json
        { "param": "/test" }
	EOF

    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
    ENVIRONMENT=unittest \
        $(dirname $0)/../run_api.sh testcollection testrequest

    verify_expectation '{"httpRequest":{"path":"/test"},"times":{"atLeast":1,"atMost":1}}'
}

_test_default_endpoint() {
    set_expectation '{"httpRequest":{"path":"/"},"httpResponse":{"body":"\"ok\""},"times":{"unlimited":true}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "unittest": {
                "url": "${MOCK_SERVER_URL:-http://localhost:1080}"
            }
        };

        def testrequest:
        {};
	EOF

    cat <<- EOF > ${tmpcollectiondir}/profiles/testprofile.json
        {}
	EOF

    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
    ENVIRONMENT=unittest \
        $(dirname $0)/../run_api.sh testcollection testrequest testprofile

    verify_expectation '{"httpRequest":{"path":"/"},"times":{"atLeast":1,"atMost":1}}'
}

_test_parameter() {
    set_expectation '{"httpRequest":{"path":"/test/1"},"httpResponse":{"body":"\"ok\""},"times":{"unlimited":true}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "unittest": {
                "url": "${MOCK_SERVER_URL:-http://localhost:1080}"
            }
        };

        def testrequest:
        {
            "endpoint": "/test/\\(\$profile[0].param)"
        };
	EOF

    cat <<- EOF > ${tmpcollectiondir}/profiles/testprofile.json
        {
            "param": 1
        }
	EOF

    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
    ENVIRONMENT=unittest \
        $(dirname $0)/../run_api.sh testcollection testrequest testprofile

    verify_expectation '{"httpRequest":{"path":"/test/1"},"times":{"atLeast":1,"atMost":1}}'
}

_test_execution() {
    set_expectation '{"httpRequest":{"path":"/test"},"httpResponse":{"body":"\"ok\""},"times":{"unlimited":true}}'
    cat <<- EOF > ${tmpcollectiondir}/testcollection.jq
        def _envs:
        {
            "unittest": {
                "url": "${MOCK_SERVER_URL:-http://localhost:1080}"
            }
        };

        def testrequest:
        {
            "endpoint": "/test"
        };
	EOF

    cat <<- EOF > ${tmpcollectiondir}/profiles/testprofile.json
        {}
	EOF

    COLLECTION_DIR=${tmpcollectiondir} \
    PROFILE_DIR=${tmpcollectiondir}/profiles \
    ENVIRONMENT=unittest \
        $(dirname $0)/../run_api.sh testcollection testrequest testprofile

    verify_expectation '{"httpRequest":{"path":"/test"},"times":{"atLeast":1, "atMost":1}}'
}
