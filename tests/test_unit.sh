tear_down() {
    return 0
}

set_up() {
    return 0
}

jq_exec() {
    jq -n -L$(realpath ..) "import \"process\" as p; ${@}"
}

assert() {
    [ "$(echo "${1}" | jq -e ". == ${2}")" != "true" ] && \
        echo "$(echo "$1" | jq -C '.')" && \
        echo "is not valid, expected" && \
        echo "$(echo ${2} | jq -C '.')" && \
        return 1

    return 0
}

_test_getenv(){
    local result=$(jq_exec 'p::getenv({"local": {"key": "val"}}; "local")')
    assert "$result" '{"key": "val"}'
}

_test_getenv_override_local(){
    local result=$(jq_exec 'p::getenv({"*": {"key": "testval"}, "local": {"key": "test2"}}; null)')
    assert "$result" '{"key": "test2"}'
}

_test_getenv_override(){
    local result=$(jq_exec 'p::getenv({"*": {"key": "testval"}, "env": {"key": "test2"}}; "env")')
    assert "$result" '{"key": "test2"}'
}

_test_getenv_combine(){
    local result=$(jq_exec 'p::getenv({"*": {"key": "testval"}, "env": {"key2": "test2"}}; "env")')
    assert "$result" '{"key": "testval", "key2": "test2"}'
}

_test_getenv_any(){
    local result=$(jq_exec 'p::getenv({"*": {"key": "testval"}}; "env")')
    assert "$result" '{"key": "testval"}'
}

_test_getenv_local(){
    local result=$(jq_exec 'p::getenv({"local": {"key": "testval"}}; null)')
    assert "$result" '{"key": "testval"}'
}

_test_getenv_change(){
    local result=$(jq_exec 'p::getenv({"local": {"key": "testval"}}; "local")')
    assert "$result" '{"key": "testval"}'
}

_test_process_formatter() {
    local result=$(jq_exec '{"formatter": "cat"} | p::process({"url": "http://test"})')
    assert "$result" "\"curl -sv -XGET  'http://test/' 2> >(sed '/^* /d; /bytes data]$/d; s/> //; s/< //; /^ *CApath:/ d' >&2) | cat\""
}

_test_process_browser() {
    local result=$(BROWSER=browser jq_exec '{"executor": "browser"} | p::process({"url": "http://test"})')
    assert "$result" "\"browser 'http://test/'\""
}

_test_process_heraders() {
    local result=$(jq_exec '{"headers":{"X-TEST": "test"}} | p::process({"url": "http://test"})')
    assert "$result" "\"curl -sv -XGET -H 'X-TEST: test'  'http://test/' 2> >(sed '/^* /d; /bytes data]$/d; s/> //; s/< //; /^ *CApath:/ d' >&2) | jq -C '.'\""
}

_test_process_extra() {
    local result=$(jq_exec 'p::process({"url": "http://test"}; "tee /tmp/file")')
    assert "$result" "\"curl -sv -XGET  'http://test/' 2> >(sed '/^* /d; /bytes data]$/d; s/> //; s/< //; /^ *CApath:/ d' >&2) | tee /tmp/file | jq -C '.'\""
}

_test_process_method() {
    local result=$(jq_exec '{"method": "post"} | p::process({"url": "http://test"})')
    assert "$result" "\"curl -sv -XPOST  'http://test/' 2> >(sed '/^* /d; /bytes data]$/d; s/> //; s/< //; /^ *CApath:/ d' >&2) | jq -C '.'\""
}

_test_process() {
    local result=$(jq_exec 'p::process({"url": "http://test"})')
    assert "$result" "\"curl -sv -XGET  'http://test/' 2> >(sed '/^* /d; /bytes data]$/d; s/> //; s/< //; /^ *CApath:/ d' >&2) | jq -C '.'\""
}
